# ACME Training Exercise #

This repository holds the architecture and demo data for the ACME e-commerce sites.
https://postal-code.co.uk/postcode/London

## Resources

* [Sandbox](https://lyracons-alliance-prtnr-na01-dw.demandware.net/on/demandware.store/WFS/Sites-Site)
* [ISO 639 - Language Codes](https://www.iso.org/iso-639-language-codes.html)
* [ISO Online Browsing Plaform](https://www.iso.org/obp/ui/#search)
  > Click on 'Country codes' if you don't the country list right away.


## Customer
ACME Corporation produces every product type imaginable, no matter how elaborate or extravagant —most of which never work as desired or expected (some products do work very well, but backfire against the user).

#### Business Lines:
* Jet-Propelled (categoryJetPropelled):
  * Unicycle (always available, only in Argentina)
  * Tennis Shoes (100 units available)
  * Motor (100 units available)
* Chemicals (categoryChemicals):
  * Earthquake Pills (always available, only in China)
  * Hi-Speed Tonic (100 units available)
  * Tornado Seeds (100 units available)
* Explosives (categoryExplosives):
  * TNT (always available, only in USA)
  * Detonator (100 units available)
  * Nitroglycerin (100 units available)
* Future Stuff (categoryFutureStuff):
  * A-1 Disintegrating Pistol (always available, only in GBR)
  * Ultimatum Dispatcher (100 units available)
  * Ultimatum Answerer (100 units available)


## Development Team
*   CB: UK site, Future products
*   JG: USA site, Explosive products
*   EP: China site, Chemical products
*   JT: Argentina site, Jet-Propelled products


## Requirements
#### Assumptions
  * If specs doesn't indicate that a product (or line of products) is available in a country, then the product is available globally.
  * Products available only in one country must appear in that country's only.
  * ALL products have the folowing string attributes: Height, Width, Depth and Weight

#### Locale & Currency:

##### UK
  - British English, Irish (optional),
  - Range value measurement (codename 'measureRange', include refinement definition)

##### USA:
  - American English, French (optional),
  - Energy value measurement (codename 'measureEnergy', include refinement definition)

##### China:
- American English, Chinese (optional),
- Richter Scale value measurement (codename 'measureRichter'

##### Argentina:
- Spanish (local variation), American English (optional)
- Speed measurement (codename 'measureSpeed', include refinement definition)

#### OCAPI Settings:
##### Data:
  - Search (CB): catalog, category, product
  - Customer (JG): CRUD
  - Products (EP): CRUD
  - Stores (JT): CRUD
##### Shop:
  - Product Search (CB): search
  - Product Search (JG): search availability
  - Product Search (EP): search images
  - Product Search (JT): search variations
